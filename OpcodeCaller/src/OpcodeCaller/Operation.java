package OpcodeCaller;

import com.portal.pcm.*;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import java.math.BigDecimal;

import javax.print.DocFlavor.STRING;
import javax.swing.text.AbstractDocument.Content;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.*;
import javax.xml.xpath.*;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class Operation 
{
   public static boolean interactive= true;
	
	private int ID;
	private String name;
	private String	multiResult;
	private FList inputFlist;
	private FList outputFList;
	private String filePath;
	private int   opcode;
	private int   flags;
	private int index ; //is used in multiple results to select which element should be retrieved
	private ArrayList<OpcodeCaller.OP_Field> inputFields ;
	private ArrayList<OpcodeCaller.OP_Field> outputFields ;
	public static Logger logger;
	
	static
	{
		Operation.logger=ExecutionPlan.logger;
	}
	
	public Operation()
	{
		this.ID=0;
		this.name="";
		this.inputFlist=null;
		this.outputFList=null;
		this.opcode=0;
		this.flags=0;
		this.filePath="";
		this.inputFields = new ArrayList<OP_Field>(4) ;
		this.outputFields = new ArrayList<OP_Field>(4);
		index= -1;
	}

	public  Object getValeufromsource( ArrayList<Operation> operationList ,String source)
	{
		String  vals[] = source.split(":");
		 
		try
		{
			int operationID = Integer.parseInt(vals[0]);
			String fieldName = vals[1];
			
			Operation oper= ExecutionPlan.getOperationByID( operationList,operationID);
			OP_Field fld= oper.getOutputFieldByName(fieldName);
			return fld.getFieldValue();
		}
		catch (NumberFormatException ex ) {System.out.println("invalid source format");}
		catch (IndexOutOfBoundsException ex ) {System.out.println("invalid source format");}
		return "";
	}

	public OP_Field getOutputFieldByName(String name )
	{
		for(int i=0 ; i < outputFields.size(); i++ )
		{
			OpcodeCaller.OP_Field currrentField =outputFields.get(i);
			if( currrentField.getName().equals(name))
				return currrentField;
				
		}
		return null;
	}
	
	public boolean parseInputFlist()
	{
		File inputFile = new File(this.filePath);
		FList parsedFlist = null;
		

		if (!inputFile.exists())
			{
				System.out.println(inputFile.getAbsolutePath() + " doesn't exist" );
				return false;
			}
		
		try
		{
			BufferedReader reader = new BufferedReader( new FileReader(inputFile));
			String line = null;
			StringBuilder strBulider= new StringBuilder();
			 while( ( line = reader.readLine() ) != null )
			 {
			        strBulider.append( line  );
			        strBulider.append( "\n");
			 }
			 
			 parsedFlist = FList.createFromString(strBulider.toString());
 
			 this.inputFlist = parsedFlist;
		}
		catch (FileNotFoundException e)
		{
			System.out.println(e.getMessage() );
			e.printStackTrace();
			return false;
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage() );
			e.printStackTrace();
			return false;
		}
		catch (EBufException e)
		{
			System.out.println(e.getMessage() );
			e.printStackTrace();
			return false;
		}
		
		
		
		return true;
	}
	
	public boolean populateInputfieldsInFlist()
	{
		
		
		for (int i =0; i< this.inputFields.size();i++)
		{
			OP_Field currentField = this.inputFields.get(i);
			Object fieldValue=currentField.getFieldValue();
			if (fieldValue.equals("?"))
			{
				System.out.println("Enter field <"+ currentField.getName()+"> "+ "of operation "+this.getName()+":"+ this.getID() );
				fieldValue=ExecutionPlan.readInput();
			}
			
			if(fieldValue.getClass() ==  String.class )
			{
				setField(this.inputFlist, this.inputFields.get(i).getXpath(), (String)fieldValue);
			}
			else
			{
				setField(this.inputFlist, this.inputFields.get(i).getXpath(), fieldValue);
			}
		}

		return true;
	}
	

	public boolean getOutputFieldsFromFlist()
	{
		boolean returnValue= true;
		boolean display= false;
		if(interactive)
		{
			System.out.println("Print output fields of operation:" +this.getName()+":"+ this.getID() + "? (y/n)"  );
			if ( true ) //ExecutionPlan.readInput().equals("y")
			{
				display=true;
			}
		}
		
		if (this.multiResult != null && this.multiResult.equalsIgnoreCase("y") )
		{
			System.out.println(this.outputFList.asString());
			System.out.print("please enter the selected index or enter -1 to use the index in config file: ");
			String indexStr = ExecutionPlan.readInput();
			this.index = Integer.parseInt(indexStr);
			
		}
		
		for (int i =0; i< this.outputFields.size();i++)
		{
			OP_Field currentField=this.outputFields.get(i);
			Object fieldValue= getField(this.getOutputFList(), currentField.getXpath(),this.index);
			
			if (fieldValue == null)
			{
				ExecutionPlan.logger.log(Level.SEVERE, "Unable to fetch  output field <"+ currentField.getName()+" >from flist");
				returnValue =  false;
			}
			else
			{
				currentField.setFieldValue(fieldValue);
				returnValue =  true;
				if (display)
					System.out.println("field <" + currentField.getName()+"> "+ fieldValue);
			}
		}
		
		return returnValue;
	}
	

	public boolean executeOperation (PortalContext cntx )
	{
		
		try
		{
			logger.log(Level.INFO, "Input flist of operation:"+this.getName()+":"+ this.getID());
			logger.log(Level.INFO, this.inputFlist.asString());
			this.outputFList = cntx.opcode(this.opcode,this.flags, this.inputFlist);
			//System.out.print(outputFList.asString());
			if(this.outputFList != null)
			{
				logger.log(Level.INFO, "Output flist of operation:"+this.getName()+":"+ this.getID());
				logger.log(Level.INFO, this.outputFList.asString());
			}
		}
		catch (EBufException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void parseFieldsWithSource(ArrayList<Operation> operationList)
	{
		for(int i=0; i< this.inputFields.size(); i++)
		{
			OP_Field fld = inputFields.get(i);
			Object value="";
			if (fld.getSource() != null)
			{
				if(!fld.getSource().equals(""))
				{
					value = getValeufromsource(operationList, fld.getSource());
					fld.setFieldValue(value);
				}
				
			}
			
		}
	}
	
	
	
	static Object getField(FList inputFlist, String path ,int index)
	{
		//path.replace('.', ':');
		
		
		String[] fields = path.split("\\.");
		FList currentFlist = inputFlist; 
		
		for(int i=0; i< fields.length; i++)
		{
			String[] tmp;
			tmp = fields[i].split("\\[");
			fields[i] = tmp[0];
			if(tmp.length == 2)
			{
				// to delete ']'
				tmp[1] = tmp[1].substring(0, tmp[1].length()-1);
				try{
					if(index == -1 )
					{
						index = Integer.parseInt(tmp[1]);
					}
				}
				catch(NumberFormatException ex)
				{
					System.out.println(path + ": invalid path");
					return null;
				}
			}
				
			//System.out.println(fields[i] + " <<index >> " + index);
			
			Field currentField = Field.fromPINName(fields[i]);
			if(currentField == null)
			{
				System.out.println("getField: " + path +" not found in flist");

				return null;
			}
			if(currentField.getTypeID() == Field.TYPE_ARRAY)
			{
				try
				{
					currentFlist = currentFlist.getElement((ArrayField) currentField, index);
				}
				catch (EBufException e)
				{
					System.out.println("getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, "getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, e.toString());
					return null;
				}
				if(currentFlist == null)
				{
					System.out.println("getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, "getField: unable to retrieve field " + path);
					return null;
				}
				
			}
			
			else if(currentField.getTypeID() == Field.TYPE_SUBSTRUCT)
			{
				try
				{
					currentFlist = currentFlist.get((SubStructField)currentField);
				}
				catch (EBufException e)
				{
					System.out.println("getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, "getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, e.toString());
					return null;
				}
				if(currentFlist == null)
				{
					System.out.println("getField: unable to retrieve field " + path);
					logger.log(Level.SEVERE, "getField: unable to retrieve field " + path);
					return null;
				}
			}
			else 
			{
				Object returnValue = currentFlist.get(currentField);
				if(returnValue == null)
				{
					System.out.println("getField: " + path +" not found in flist");
					return null;
				}
				
				//System.out.println("getField: " + currentField.getPINType()+ "<"+ returnValue.toString()+">");
				return returnValue;
			}
			
		}
		
		return null;
	}

	static boolean setField(FList inputFlist, String path , Object inputValue )
	{	
		
		if(inputValue == null)
		{
			System.out.println("setField: value of " +path+ " is null");
			return false;
		}
		
		String[] fields = path.split("\\.");
		FList currentFlist = inputFlist; 
		
		for(int i=0; i< fields.length; i++)
		{
			int index=-1;
			String[] tmp;
			tmp = fields[i].split("\\[");
			fields[i] = tmp[0];
			if(tmp.length == 2)
			{
				// to delete ']'
				tmp[1] = tmp[1].substring(0, tmp[1].length()-1);
				try{
					index = Integer.parseInt(tmp[1]);
				}
				catch(NumberFormatException ex)
				{
					System.out.println(path + ": invalid path");
					return false;
				}
			}

				
			//System.out.println(fields[i] + " <<index >> " + index);
			
			Field currentField = Field.fromPINName(fields[i]);
			if(currentField == null)
			{
				System.out.println("setField: " + path +" invalid field");

				return false;
			}
			
			if(currentField.getTypeID() == Field.TYPE_ARRAY)
			{
				FList ParrentElementFlist = null;
				if (index < 0 )
				{
					System.out.println(path + ": invalid path");
					return false;
				}
				
				try
				{
					ParrentElementFlist = currentFlist.getElement((ArrayField) currentField, index);
				}
				catch (EBufException e) {}
				
				if (ParrentElementFlist == null)
				{
					ParrentElementFlist = new FList(); 
					currentFlist.setElement((ArrayField) currentField, index, ParrentElementFlist);
				}
				currentFlist = ParrentElementFlist;
			
			
			}
			else if(currentField.getTypeID() == Field.TYPE_SUBSTRUCT)
			{
				FList ParrentSubstructFlist = null;
				try
				{
					ParrentSubstructFlist = currentFlist.get((SubStructField)currentField);
				}
				catch (EBufException e)	{}
				
				if (ParrentSubstructFlist == null)
				{
					ParrentSubstructFlist = new FList();
					currentFlist.set((SubStructField) currentField, ParrentSubstructFlist);
				}
				currentFlist = ParrentSubstructFlist;
				
			}
			else 
			{				
				switch (currentField.getTypeID())
				{
				case Field.TYPE_DECIMAL:
					currentFlist.set((DecimalField) currentField, (BigDecimal) inputValue );
					break;
					
				case Field.TYPE_ENUM:
					currentFlist.set((IntField) currentField, (Integer) inputValue);
					break;
					
				case Field.TYPE_INT:
					currentFlist.set((IntField) currentField, (Integer) inputValue);
					break;
					
				case Field.TYPE_UINT:
					currentFlist.set((IntField) currentField, (Integer) inputValue);
					break;
					
				case Field.TYPE_OBJ:
					currentFlist.set((ObjField) currentField,(Poid) inputValue);
					break;
					
				case Field.TYPE_POID:
					currentFlist.set((PoidField) currentField, (Poid) inputValue);
					break;
					
				case Field.TYPE_STR:
					currentFlist.set((StrField) currentField, (String) inputValue);
					break;
				case Field.TYPE_TSTAMP:
					currentFlist.set((TStampField) currentField, (Date) inputValue);
					break;
				default:
					System.out.println("unable to define the datatype of the field");
					break;
				}
				
				return true;
			}

		}
			
		
		
		return false;
	}

	static boolean setField(FList inputFlist, String path , String inputValueAsString )
	{	
		if(inputValueAsString == null)
		{
			System.out.println("setField: value of " +path+ " is null");
			return false;
		}
		
		String[] fields = path.split("\\.");
		FList currentFlist = inputFlist; 
		
		for(int i=0; i< fields.length; i++)
		{
			int index=-1;
			String[] tmp;
			tmp = fields[i].split("\\[");
			fields[i] = tmp[0];
			if(tmp.length == 2)
			{
				// to delete ']'
				tmp[1] = tmp[1].substring(0, tmp[1].length()-1);
				try{
					index = Integer.parseInt(tmp[1]);
				}
				catch(NumberFormatException ex)
				{
					System.out.println(path + ": invalid path");
					return false;
				}
			}
			
			//System.out.println(fields[i] + " <<index >> " + index);
			
			Field currentField = Field.fromPINName(fields[i]);
			if(currentField == null)
			{
				System.out.println("setField: " + path +" invalid field");

				return false;
			}
			
			if(currentField.getTypeID() == Field.TYPE_ARRAY)
			{
				FList ParrentElementFlist = null;
				if (index < 0 )
				{
					System.out.println(path + ": invalid path");
					return false;
				}
				
				try
				{
					ParrentElementFlist = currentFlist.getElement((ArrayField) currentField, index);
				}
				catch (EBufException e)	{}
				if (ParrentElementFlist == null)
				{
					ParrentElementFlist = new FList(); 
					currentFlist.setElement((ArrayField) currentField, index, ParrentElementFlist);
				}
				currentFlist = ParrentElementFlist;
				
			}
			
			else if(currentField.getTypeID() == Field.TYPE_SUBSTRUCT)
			{
				FList ParrentSubstructFlist = null;
				try
				{
					ParrentSubstructFlist = currentFlist.get((SubStructField)currentField);
				}
				catch (EBufException e)	{}
				if (ParrentSubstructFlist == null)
				{
					ParrentSubstructFlist = new FList();
					currentFlist.set((SubStructField) currentField, ParrentSubstructFlist);
				}
				currentFlist = ParrentSubstructFlist;
				
			}
			else 
			{
				
				
				switch (currentField.getTypeID())
				{
				case Field.TYPE_DECIMAL:
					currentFlist.set((DecimalField) currentField, BigDecimal.valueOf(Double.parseDouble(inputValueAsString)) );
					break;
					
				case Field.TYPE_ENUM:
					currentFlist.set((IntField) currentField, Integer.valueOf(inputValueAsString));
					break;
					
				case Field.TYPE_INT:
					currentFlist.set((IntField) currentField, Integer.valueOf(inputValueAsString));
					break;
					
				case Field.TYPE_UINT:
					currentFlist.set((IntField) currentField, Integer.valueOf(inputValueAsString));
					break;
					
				case Field.TYPE_OBJ:
					currentFlist.set((ObjField) currentField, Poid.valueOf(inputValueAsString));
					break;
					
				case Field.TYPE_POID:
					currentFlist.set((PoidField) currentField, Poid.valueOf(inputValueAsString));
					break;
					
				case Field.TYPE_STR:
					currentFlist.set((StrField) currentField, inputValueAsString);
					break;
				case Field.TYPE_TSTAMP:
					currentFlist.set((TStampField) currentField, new Date(Long.parseLong(inputValueAsString)));
					break;
				default:
					System.out.println("unable to define the datatype of the field");
					break;
				}
				
				return true;
			}
			
		}
		
		return false;
	}

	

	public FList getInputFlist() {
		return inputFlist;
	}

	public void setInputFlist(FList inputFlist) {
		this.inputFlist = inputFlist;
	}

	public FList getOutputFList() {
		return outputFList;
	}

	public void setOutpFList(FList outpFList) {
		this.outputFList = outpFList;
	}

	public int getOpcode() {
		return opcode;
	}

	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		this.ID = iD;
	}

	public ArrayList<OpcodeCaller.OP_Field> getInputFields() {
		return inputFields;
	}

	public void setInputFields(ArrayList<OpcodeCaller.OP_Field> inputFields) {
		this.inputFields = inputFields;
	}

	public ArrayList<OpcodeCaller.OP_Field> getOutputFields() {
		return outputFields;
	}

	public void setOutputFields(ArrayList<OpcodeCaller.OP_Field> outputFields) {
		this.outputFields = outputFields;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMultiResult() {
		return multiResult;
	}

	public void setMultiResult(String multiResult) {
		this.multiResult = multiResult;
	}
	
	
}
