package OpcodeCaller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;

public class Main
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		File selectedPlan = ExecutionPlan.readPlans();		
		ExecutionPlan plan = ExecutionPlan.loadExecutionPlanConfig(selectedPlan);
		plan.executePlanFlow();
	}
	
	   public static String readInput() 
	   {
		   BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	        
	        String inStr = "";
	        try
			{
				inStr = reader.readLine();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return inStr;
	   }

}
