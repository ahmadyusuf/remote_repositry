package OpcodeCaller;

public class OP_Field
{

	private String Xpath;
	private Object fieldValue;
	private String name;
	private String source;
	
	public String getXpath() {
		return Xpath;
	}
	public void setXpath(String xpath) {
		this.Xpath = xpath;
	}
	public Object getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	
}

