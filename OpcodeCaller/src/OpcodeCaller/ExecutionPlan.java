package OpcodeCaller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.portal.pcm.*;

public class ExecutionPlan {
	
	private boolean isTransaction;
	private ArrayList<Operation> opcodeList;
	
	
	   public  static Logger logger;
	   public static boolean interactive= false;
	   static PortalContext context;
	   
	   static Properties parameters;
	   
	   /*########## Log Level ###############
	   #    0 = Error 
	   #    1 = Debug 
	   ####################################*/    
	   
	   
	   static public int logLevel ;
	   static public File inputFile;
	   /*
	  
	    0 = Error 
	    1 = Debug 
	    
	    */
	   
		
	   static
	   {
		   configLog();
		   parameters = loadParameters();
		 
		   connect(parameters); //to initiate connection to infranet using context portal
	    
	   } 
	      
	   
	   public static void configLog()
	   {
	        try
	          {

	        	
	            logger = Logger.getLogger("OpcodeCaller");
	            logger.setUseParentHandlers(false);
	            FileHandler fh = new FileHandler("OpcodeCaller.log", true);
	            SimpleFormatter formatter = new SimpleFormatter();
	            fh.setFormatter(formatter);
	            logger.addHandler(fh);
	            logger.setLevel(Level.ALL);
	     

	          } catch (IOException ex)
	          {
	        	ex.printStackTrace();  
	            logger.log(Level.SEVERE,ex.getMessage());    
	            System.out.println("Log file not found");
	            ex.printStackTrace();
	            System.exit(1);
	          } catch (SecurityException ex)
	          {
	              logger.log(Level.SEVERE,ex.getMessage());  
	            ex.printStackTrace();
	            System.exit(1);
	          }


	      }

	   public static void connect(Properties parameters) 
	   {

	        try
	          {
	           
	        	context=new PortalContext();
	        	context.connect(parameters);
	            logger.log(Level.INFO,"Connected to BRM CM successfully");

	          } catch (Exception ex)
	          {
	              
	             logger.log(Level.SEVERE,"Can't connect to BRM CM");
	             logger.log(Level.SEVERE,ex.getMessage());  
	             System.exit(1);
	          }
	    }
	   
	   public static Properties loadParameters()
	   {
		   Properties parameters = new Properties();
		   
		   try 
		   {
			parameters.load(new FileInputStream("Infranet.properties"));
					
		   } 
	       catch (FileNotFoundException ex)
	       {
	    	logger.log(Level.SEVERE,"Unable to access Infranet.properties ");   
	        logger.log(Level.SEVERE,ex.getMessage());
	        System.exit(1);
	       }
	      catch (IOException ex)
	       {
	        logger.log(Level.SEVERE,ex.getMessage());
	        System.exit(1);
	       }
		   
		   
		   if(!parameters.containsKey("logLevel") )
		   {
			   logger.log(Level.WARNING,"Infranet.properties doesn't contain logLevel Parameter \n Default logLevel value is 0 (error)");
			   logLevel=0;
			   
		   }
		   else
		   {
			  try{   
				  logLevel  = Integer.parseInt(parameters.getProperty("logLevel"));
				  if (!(logLevel==0 || logLevel==1) )
					  throw new Exception();
				  
				  }
			  catch (Exception ex){
				  logger.log(Level.SEVERE,"logLevel has invalid value. only 0 or 1 are acceptable");
				  System.exit(1); 
			  }
		   }

	
		 return parameters;
	    

	   }
		
	   public static String readInput() 
	   {
		   BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	        
	        String inStr = "";
	        try
			{
				inStr = reader.readLine();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return inStr;
	   }
	   
	
	
	public ArrayList<Operation> getOpcodeChain() {
		return opcodeList;
	}
	public void setOpcodeChain(ArrayList<Operation> opcodeChain) {
		this.opcodeList = opcodeChain;
	}
	public boolean isTransaction() {
		return isTransaction;
	}
	public void setTransaction(boolean isTransaction) {
		this.isTransaction = isTransaction;
	}
	
	
	public  ExecutionPlan ()
	{
		this.isTransaction=false;
		this.opcodeList= new ArrayList<Operation>(5);
	}
	

	public static Operation getOperationByID( ArrayList<Operation> operationList, int id)
	{
		for(int i=0 ; i < operationList.size(); i++ )
		{
			Operation currentOp =operationList.get(i);
			if( currentOp.getID() == id)
				return currentOp;
				
		}
		return null;
	}
	
	public void executePlanFlow()
	{
		boolean result = false;
		Operation opcode=null;
		for(int i=0; i<this.opcodeList.size(); i++)
		{

			opcode=this.opcodeList.get(i);
			logger.log(Level.INFO, "excute operation: " +opcode.getName()+":"+ opcode.getID());
			opcode.parseFieldsWithSource(this.opcodeList);	
			
			result = opcode.parseInputFlist();
			if (!result)
			{
				logger.log(Level.INFO, "Application failed to parse the input flist of  operation: " +opcode.getName()+":"+ opcode.getID());
				System.exit(-1);
			}
			result =false;
			
			if(interactive)
			{
				System.out.println("Print intput flist as XML , operation:" +opcode.getName()+":"+ opcode.getID() + "? (y/n)"  );
				if ( ExecutionPlan.readInput().equals("y") )
				{
					try
					{
						System.out.println(opcode.getInputFlist().toXMLString());
					}
					catch (EBufException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		
			result = opcode.populateInputfieldsInFlist();
			if (!result)
			{
				logger.log(Level.INFO, "Application failed to populate the input flist with the xpath input fields of operation: "+opcode.getName()+":"+ opcode.getID());
				System.exit(-1);
			}			
			result =false;
			
			if(interactive)
			{
				System.out.println("Print intput flist after populating the fields of operation: " +opcode.getName()+":"+ opcode.getID() + "? (y/n)"  );
				if ( ExecutionPlan.readInput().equals("y") )
				{
					System.out.println(opcode.getInputFlist().asString());
				}
			}	
			System.out.println("Execute operation: " +opcode.getName()+":"+ opcode.getID() + "? (y/n)"  );
			if ( ExecutionPlan.readInput().equals("y") )
			{
				result = opcode.executeOperation(context);
			}
			else 
				result = false;
	
			
			
			if (!result)
			{
				logger.log(Level.INFO, "Application failed to excute the opcode of operation: " +opcode.getName()+":"+ opcode.getID());
				System.exit(-1);
			}		
			
			result =false;
			if(interactive)
			{
				System.out.println("Print output flist of operation:" +opcode.getName()+":"+ opcode.getID() + "? (y/n)"  );
				if ( ExecutionPlan.readInput().equals("y") )
				{
					System.out.println(opcode.getOutputFList().asString());
				}
			}
			
			result = opcode.getOutputFieldsFromFlist();
			if (!result)
			{
				logger.log(Level.INFO, "Application failed to extract fields from output flist of  operation: " +opcode.getName()+":"+ opcode.getID());
				System.exit(-1);
			}	
			result =false;
		}
	}
	
	public static ExecutionPlan loadExecutionPlanConfig(File xmlConfigFile)
	{
		ExecutionPlan plan = new ExecutionPlan();
		File xmlFile = xmlConfigFile;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc;
		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			Node root=  doc.getDocumentElement();
		
			Attr transaction = (Attr) root.getAttributes().getNamedItem("transaction");
			if (transaction.equals("true"))
			{
				plan.isTransaction=true;
			}
			
			//*********************************************************************
			//****************************Operations**************************
			//*********************************************************************
			NodeList operationNodeList = root.getChildNodes();
			Operation operationObj=null; 
			Node operationNode=null; 
			for (int i = 0; i < operationNodeList.getLength(); i++) 
			{
	  			if (operationNodeList.item(i).getNodeType() != Node.ELEMENT_NODE  )
	  				continue;
				operationObj = new Operation();
				operationNode =  operationNodeList.item(i);
				if (operationNode.getNodeName().equals("operation"))
				{
				   Attr attTmp = (Attr) operationNode.getAttributes().getNamedItem("id");
				   int tmpID = Integer.parseInt(attTmp.getValue());
				   operationObj.setID(tmpID);
				 
				   attTmp = (Attr) operationNode.getAttributes().getNamedItem("name");
				   if(attTmp != null)
				   {
					   operationObj.setName(attTmp.getValue());
				   }
				   
				   attTmp = (Attr) operationNode.getAttributes().getNamedItem("multiResult");
				   if(attTmp != null)
				   {
					   operationObj.setMultiResult(attTmp.getValue());
				   }
				   
				//	NodeList operationChildNodeList = operationNode.getChildNodes();
					
					Element operationElement = (Element) operationNode ;
					int opcode = Integer.parseInt(getTagValue("opcode", operationElement));
					operationObj.setOpcode(opcode);
					int flags = Integer.parseInt(getTagValue("flags", operationElement));
					operationObj.setFlags(flags);
					
					String  filePath =getTagValue("file_path", operationElement);
					operationObj.setFilePath(filePath);
					//**********************************************************************
					//************************* input Fields*****************************					
					//**********************************************************************					
					Node FieldsNode = operationElement.getElementsByTagName("input_fields").item(0);
					NodeList  FieldsNodeList = FieldsNode.getChildNodes();
					OP_Field currentField = null;
					for (int fieldIndex = 0; fieldIndex < FieldsNodeList.getLength(); fieldIndex++) 
					{
			  			if (FieldsNodeList.item(fieldIndex).getNodeType() != Node.ELEMENT_NODE  )
			  				continue;
			  			if (FieldsNodeList.item(fieldIndex).getNodeName().equals("field"))
			  			{
				  			currentField = new OP_Field();
				  			Element inputFieldsElement =  (Element) FieldsNodeList.item(fieldIndex);
				  			String name = inputFieldsElement.getAttributeNode("name").getValue() ;
				  			if(name != null)
				  				currentField.setName( name );
				  			
				  			currentField.setXpath( inputFieldsElement.getAttributeNode("path").getValue()  );
				  			currentField.setFieldValue(inputFieldsElement.getChildNodes().item(0).getNodeValue().trim() );
				  			
				  			if(inputFieldsElement.getAttributeNode("source")!=null)
				  			{
				  				Attr source = inputFieldsElement.getAttributeNode("source");
				  				currentField.setSource(source.getValue()  );
				  			}
				  			
				  			operationObj.getInputFields().add(currentField);
			  			}
					}
									
				//**********************************************************************
				//*************************Output Fields*****************************					
				//**********************************************************************					
					FieldsNode = operationElement.getElementsByTagName("output_fields").item(0);
					FieldsNodeList = FieldsNode.getChildNodes();
					currentField = null;
					for (int fieldIndex = 0; fieldIndex < FieldsNodeList.getLength(); fieldIndex++) 
					{
			  			if (FieldsNodeList.item(fieldIndex).getNodeType() != Node.ELEMENT_NODE  )
			  				continue;
			  			if (FieldsNodeList.item(fieldIndex).getNodeName().equals("field"))
			  			{
				  			currentField = new OP_Field();
				  			Element inputFieldsElement =  (Element) FieldsNodeList.item(fieldIndex);
				  			
				  			currentField.setXpath( inputFieldsElement.getAttributeNode("path").getValue()  );
				  			currentField.setName(inputFieldsElement.getAttributeNode("name").getValue()  );
				  			currentField.setFieldValue(inputFieldsElement.getChildNodes().item(0).getNodeValue().trim() );
				  			
				  			operationObj.getOutputFields().add(currentField);
			  			}
					}					
					
					
					//**********************************************************************
					//*************************Output Fields*****************************					
					
					
					plan.opcodeList.add(operationObj);
				   
				}
	
			}
			
			
			//****************************End of Operations*****************************
			//*********************************************************************
			
				
			return plan;
			
			
		}
		catch (ParserConfigurationException e)
		{
			e.getMessage();
			e.printStackTrace();
		}		
		catch (SAXException e)
		{
			e.getMessage();
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.getMessage();
			e.printStackTrace();
		}
		
		
		
		return null;
	}
	
	private static String getTagValue(String sTag, Element eElement) 
	{
			NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
			Node nValue = (Node) nlList.item(0);
			return nValue.getNodeValue();
	}
	 
	
	public static File readPlans()
	{
		File executionPlanCofigDir = new File("./ExecutionPlanConfig");
		File[] filelist = executionPlanCofigDir.listFiles();
		
		System.out.println("List of the available plan you want to execute:");
		for (int i = 0; i < filelist.length; i++)
		{
			if (filelist[i].isFile())
			{
				System.out.println(i +" ==> "+ filelist[i].getName() );
			}
		}
		System.out.print("Select a plan:");
		String planIndexStr = ExecutionPlan.readInput();
		int planIndex = -1;
		try
		{
			planIndex = Integer.parseInt(planIndexStr);
		}
		catch (NumberFormatException e) 
		{
			System.out.println("Invalid Choice");
			System.exit(5);
		}
			
		if (planIndex < 0 || planIndex > (filelist.length-1))
		{
			System.out.println("Invalid Choice");
			System.exit(5);
		}
		
		return filelist[planIndex];

	}

}
